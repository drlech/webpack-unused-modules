#!/usr/bin/env node

const getArguments = require('../lib/getArguments');
const findUnusedModules = require('../lib/findUnusedModules');

findUnusedModules(getArguments());
