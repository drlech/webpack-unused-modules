const webpack = require('webpack');

const defaults = require('./defaults');
const getWebpackConfig = require('./getWebpackConfig');
const getModules = require('./getModules');
const relativeToAbsolute = require('./relativeToAbsolute');
const getModulesFromStats = require('./getModulesFromStats');
const arrayDifference = require('./arrayDifference');

module.exports = (options = {}) => {
    const optionsList = Object.assign({}, defaults, options);
    const webpackConfig = getWebpackConfig();

    // Find local components.
    let components = getModules(webpackConfig.context);

    // User may want to filter the modules out.
    if (optionsList.ignore) {
        const filter = new RegExp(optionsList.ignore);
        components = components.filter(name => !filter.test(name));
    }

    // Run webpack, get list of modules.
    webpack(webpackConfig, (err, stats) => {
        const usedModules = relativeToAbsolute(
            webpackConfig.context,
            getModulesFromStats(stats)
        );

        const unused = arrayDifference(usedModules, components);
        unused.map(item => console.log(item));
    });
}
