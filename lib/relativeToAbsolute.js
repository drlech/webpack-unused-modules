const path = require('path');

/**
 * Convert the list of relative paths to absolute paths.
 *
 * @param {string} context Directory the items are relative to.
 * @param {string[]} items List of items to convert.
 */
module.exports = (context, items) => {
    return items.map(item => path.resolve(context, item));
};
