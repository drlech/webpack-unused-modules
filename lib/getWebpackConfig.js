const path = require('path');

/**
 * Retrieve Webpack configuration from webpack.config.js
 * (from the project root).
 *
 * @returns {object}
 */
module.exports = () => {
    // Use webpack config in the root of the project.
    const webpackConfigPath = path.resolve(process.cwd(), 'webpack.config.js');
    let webpackConfig = require(webpackConfigPath);
    if (typeof webpackConfig === 'function') {
        webpackConfig = webpackConfig(process.env, process.argv);
    }

    return webpackConfig;
};
