/**
 * Find all arguments passed via command line.
 * Arguments are in the following format:
 * --name=value
 *
 * Additionally, arrays can be passed like so:
 * --name=[value1,value2,value3]
 *
 * @returns {object}
 */
module.exports = () => {
    const foundArguments = {};

    for (let i = 0; i < process.argv.length; i++) {
        const candidate = process.argv[i];
        const match = candidate.match(/--(\w+)=(.+)/);
        if (!match) {
            continue;
        }

        const name = match[1];
        let value = match[2];

        // Check if value is an array.
        const arrayMatch = value.match(/\[(.+)\]/);
        if (arrayMatch) {
            value = arrayMatch[1].split(',').map(entry => entry.trim());
        }

        foundArguments[name] = value;
    }

    return foundArguments;
};
