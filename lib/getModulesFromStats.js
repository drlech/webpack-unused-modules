/**
 * Parse stats objects from Webpack and retrieve the names
 * of all modules used, except the ones installed via NPM.
 *
 * @param {object} stats Stats object from Webpack call callback.
 *
 * @returns {string[]}
 */
module.exports = (stats) => {
    return stats.toJson().modules
        // "id" contains relative paths to the modules.
        .map(module => module.id)

        // We'll just check our own modules, so ignore stuff from NPM.
        .filter(module => !/node_modules/.test(module));
};
