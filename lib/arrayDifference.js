/**
 * Get all items from second array that are NOT present
 * in the first array.
 *
 * @param {string[]} allItems
 * @param {string[]} toCheck
 */
module.exports = (allItems, toCheck) => {
    return toCheck.filter(item => allItems.indexOf(item) === -1);
};
