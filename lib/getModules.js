const path = require('path');
const glob = require('glob');

/**
 * Generate the list of all local modules in the given directory.
 *
 * @param {string} location Where to look for modules.
 * @param {string[]} extensions Extensions to look for.
 */
module.exports = (location, extensions = ['js', 'jsx']) => {
    return glob.sync(`${location}/**/*.@(${extensions.join('|')})`)
        // Make sure the slashes in the path are consistent
        // with other arrays of paths returned by other functions.
        .map(component => path.resolve(component));
};
